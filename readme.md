---
title: 'A _dragon_ curve'  
author: 'Winter 2018'  
header-includes:  
  - \usepackage{caption}  
  - \usepackage[normalem]{ulem}  
  - \usepackage{letltxmacro}  
---
\begin{figure}[!h]
  \centering
  \captionsetup{justification=centering}
    %\includegraphics[width=0.75\textwidth]{./pics/manolo-vivo.png}
    \href{./media/TheApologySong.mp3}{\XeTeXLinkBox{
    \includegraphics[width=0.86\textwidth]{./pics/toros-y-torote.png}}}
    \caption{
      \emph{Manolo S\'anchez} about to fight \emph{``a whole lot of bull''}
    }\label{fig:1}
\end{figure}

## About the `.pdf` version of this document

If you are reading the online [markdown] version of this document, [click
here][assignment-pdf] to access the location where the `.pdf` version is hosted;
then click on the `view raw` link to download a copy of this document.[^if-you]

[^if-you]: The embedded link does not work in the `.pdf` document.

## The backstory

In order to come back from the land of the forgotten (Oh, yes! _Manolo_ found a
way to get there), _Manolo_ issued a challenge to _Xibalba_. If he wins, _La
Catrina_, _Xibalba_, and _The Candle Maker_ will send him back to \emph{San
\'Angel}. If he loses, not only will _Xibalba_ become the ruler of both realms:
_The Land of the Remembered_ and _The Land of the Forgotten_, but also _Manolo_
will be forgotten. In order to win, _Manolo_ has to become a 'matador' and will
have to: "**_defeat every bull the _** \textbf{\emph{S\'anchez}} _**family ever
finished...  All... at once.**_"

Oh, boy! If only there was a way he could tackle a smaller problem instead... 


## The assignment

I do not have a big bull for you to fight, but I do have a _Dragon..._ A _dragon
curve_ that is! 

According to wikipedia: 

> "_A dragon curve is any member of a family of self-similar fractal curves,
> which can be approximated by recursive methods such as Lindenmayer systems._"

From the same wikipedia entry:

> "_Tracing an iteration of the Heighway dragon curve from one end to the other,
> one encounters a series of 90 degree turns, some to the right and some to the
> left. For the first few iterations the sequence of right `(R)` and left `(L)`
> turns is as follows:_
>
> > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> > 1st iteration: R
> > 2nd iteration: R R L
> > 3rd iteration: R R L R R L L
> > 4th iteration: R R L R R L L R R R L L R L L.
> > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
>
> _This suggests the following pattern: each iteration is formed by taking the
> previous iteration, adding an `R` at the end, and then taking the original
> iteration again, flipping it retrograde, swapping each letter and adding the
> result after the `R`._
>
> _This pattern in turn suggests the following method of creating models of
> iterations of the Heighway dragon curve by folding a strip of paper. Take a
> strip of paper and fold it in half to the right. Fold it in half again to the
> right. If the strip was opened out now, unbending each fold to become a 90
> degree turn, the turn sequence would be `R R L` i.e., the second iteration of
> the Heighway dragon. Fold the strip in half again to the right, and the turn
> sequence of the unfolded strip is now `R R L R R L L` -- the third iteration
> of the Heighway dragon._" (See figure \ref{paper-strip}.)

\begin{figure}[ht]
  \centering
  \captionsetup{justification=centering}
    \includegraphics[width=0.86\textwidth]{./pics/dragon_curve.png}
    \caption{
      Iterations of the \emph{Heighway dragon curve} via folds in a strip of
          paper.
    }\label{paper-strip}
\end{figure}

For this assignment, your job is to create a library file (`dragon.h`)
containing the implementation of the function `generate_dragon_sequence(...)`.
This function, should receive three parameters in the following order:

*   a vector containing zero or more `bool` values [we will use `true (1)`, and
    `false (0)`, to represent _right_, and _left_, respectively];
*   an `int` that indicates the number of times our imaginary paper strip will
    be folded in half; and
*   an `ostream` object.

In order for you to receive full credit you need to implement some function in
such a way that it calls itself at some point. In other words: **it should be a
recursive implementation**. The reason why the third parameter is an `ostream`
object is so that the sequence of turns (_i.e.,_ the `1`'s or `0`'s) can either
be displayed to the console or to a file.

The file [`dragon_driver.cpp`][dragon-driver] should give you an idea of how
your library file will be tested. When I run my implementation, it produces the
output shown in figure \ref{sample-output}. In addition, the file
[`dragon.txt`][data] is created containing the sequence of turns needed to
generate a dragon curve corresponding to 5 folds of a paper strip. 

\begin{figure}[ht]
  \centering
  \captionsetup{justification=centering}
    %\includegraphics[width=0.86\textwidth]{./pics/dragon_screenshot.png}
    \href{https://www.youtube.com/watch?v=S6_-JoC8jpw}{\XeTeXLinkBox{
    \includegraphics[width=0.86\textwidth]{./pics/dragon_screenshot}}}
    \caption{
      Sample output corresponding to 3 folds of a paper strip.
    }\label{sample-output}
\end{figure}

As usual, you are free to implement this function using as many other helper
functions and/or classes you want. The only requirement is that at least one of
the functions you implement be a recursive one. Also, note that since only
one file will be collected, if you use classes, the interfaces as well as the
implementation of the member functions will have to be placed in the same file.


## What is this assignment about? 

`Recursion( Recursion( Recursion( ... ) ) )`.


## Are the sequences correct?

The easiest way to tell is to simply follow them. Use the rules below to "draw a
picture" of your program's output.

1.  On a piece of paper, draw a segment pointing up.

1.  If the previous segment points up, and 

    -   if `0` is the current bit in the sequence, then draw a segment to the
        left;
    -   else, draw a segment to the right.

    If the previous segment points down, and 
    
    -   if `0` is the current bit in the sequence, then draw a segment to the
        right;
    -   else, draw a segment to the left.

    If the previous segment points to the right, and
    
    -   if `0` is the current bit in the sequence, then draw an upward segment;
    -   else, draw a downward segment.

    If the previous segment points to the left, and

    -   if `0` is the current bit in the sequence, then draw a downward segment;
    -   draw an upward segment otherwise.

1.  Repeat the previous step for the next bit in the sequence. Stop when all
bits have been processed.

These steps can also be found in [this program][plot-curve]. When successfully
compiled and linked against the [`ccc_graphics` libraries][ccc-pic] provided by
the author of your textbook, given the file `dragon.txt` containing a sequence
of `0`'s and `1`'s, it displays a curve according to the rules described above.
If you are not familiar with these libraries and want to compile this program,
please get in touch with your TA, or come and talk to me during office hours.

The screenshots in figure \ref{curves:5-12} correspond to the sequences
generated by my implementation when the number of files is 5 and 12,
respectively. 

\begin{figure}[ht]
  \centering
  \captionsetup{justification=centering}
    \includegraphics[width=0.43\textwidth]{./pics/dragon05.png}
    \includegraphics[width=0.43\textwidth]{./pics/dragon12.png}
    \caption{
      Dragon curves corresponding to 5 and 12 folds of a paper strip.
    }\label{curves:5-12}
\end{figure}

Since you do not know how many folds I will ask your program to perform I
suggest that once you finish your project, you spend some time performing
several tests.


## Submission

Upload your completed file `dragon.h` (all lowercase) to CCLE. Notice that there
is no need to upload the driver, as I will use a slightly different one to
compile your project. _If your file is named differently, your homework **will
not be graded**_.  

Your code should contain useful comments as well as your name, the date, and a
brief description of what the program does. The files uploaded to CCLE will be
automatically collected at the date and time listed in the assignment
description (_"Grading summary"_ table located at the bottom of the page). 


## Grading rubric

|**Category**|**Description**|**Points**|
|:--------------|:--------------------------------------------------|:-------:|
| Correctness | The project compiles and the setup is as instructed. | 5 |
| Recursion | At least one of the functions is a recursive one. | 5 |
| Parameters | The parameters are as described and do not impose a computational burden. | 5 |
| Coding style | The code is efficient and easy to follow. | 5 |
| | | |
| Total | | 20 |


\newpage

If you find this assignment entertaining and if you have time available, you
might want to ~~fight a real bull~~ tackle the so called \emph{L\'evy C} curve.

\begin{figure}[ht]
  \centering
  \captionsetup{justification=centering}
    %\includegraphics[width=0.86\textwidth]{./pics/dragon_screenshot.png}
    \href{https://en.wikipedia.org/wiki/L%C3%A9vy_C_curve}{\XeTeXLinkBox{
    \includegraphics[width=0.64\textwidth]{./pics/toro_curve.png}}}
    \caption{
      I don't know about you, but to me it looks like a \emph{toro}. 
    }\label{levy-curve}
\end{figure}

## List of files

The following files are included with this assignment:

 *  Current directory:

     -  [`dragon_curve.pdf`][assignment-pdf]: this file.
     -  [`readme.md`][assignment-md]: this file's source code (a combination of
        `yaml` + `markdown` + `pandoc` + \LaTeX).
     -  [`create_pdf.sh`][script]: `pandoc` command needed to generate a `.pdf`
        file from `readme.md`. _You can safely ignore this file!_

 *  `src` directory:

     -  [`dragon_generator.cpp`][plot-curve]: can help you "graph" the sequence
        generated by your project.

 *  `drivers` directory:

     -  [`dragon-driver.cpp`][dragon-driver]: feel free to change the number of
        folds, but be aware that the length of the produced sequence grows
        exponentially.

 *  `output_files` directory:

     -  [`data.txt`][data]: sample output file. Make sure every bit in the
        sequence is in its own line by itself.

 *  `media` directory:

     -  [`TheApologySong.mp3`][apology-song]: "... I am sorry. _Toro_, I am
        sorry ..."

 *  `pics` directory:

     -  [`toros-y-torote.png`][toros]: the picture in figure \ref{fig:1}.
     -  [`dragon_curve.png`][strip]: the picture in figure \ref{paper-strip}.
     -  [`dragon_screenshot.png`][console-scrshot]: the picture in figure
        \ref{sample-output}.
     -  [`dragon05.png`][dragon5]: the picture on the left, in figure
        \ref{curves:5-12}.
     -  [`dragon12.png`][dragon12]: the picture on the right, in figure
        \ref{curves:5-12}.
     -  [`toro_curve.png`][levy]: the picture in figure \ref{levy-curve}.

[ccc-pic]: http://www.pic.ucla.edu/how-to-create-a-c-graphics-application/

[script]: create_pdf.sh
[assignment-pdf]: dragon_curve.pdf
[assignment-md]: readme.md
[dragon-driver]: drivers/dragon-driver.cpp
[plot-curve]: src/dragon_generator.cpp
[data]: output_files/data.txt
[apology-song]: media/TheApologySong.mp3
[toros]: pics/toros-y-torote.png
[strip]: pics/dragon_curve.png
[console-scrshot]: pics/dragon_screenshot.png
[dragon5]: pics/dragon05.png
[dragon12]: pics/dragon12.png
[levy]: pics/toro_curve.png


[CCLE-link]: https://ccle.ucla.edu/course/view/18W-COMPTNG10B-3?section=6
